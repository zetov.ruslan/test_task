export interface CarouselEl {
  imageUrl: string;
  title: string;
}
