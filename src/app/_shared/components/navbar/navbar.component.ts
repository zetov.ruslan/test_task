import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { AuthenticationService } from '../../services/authentication.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(
    private $modalService: NgbModal,
    private $authService: AuthenticationService
    ) { }

  open() {
    const modalRef = this.$modalService.open(ModalComponent);
    modalRef.componentInstance.name = 'World';
  }

  isLogged = () => {
    return this.$authService.isLogin();
  }

  public logOut = () => {
    this.$authService.logout();
  }
}
