import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Trip } from '../models/index';
import * as globals from '../globals';


@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(
    private $http: HttpClient
  ) { }

  public getTrips(): Observable<Trip[]> {
    return this.$http.get<Trip[]>(`${globals.$api}${globals.$trips}`);
  }

  public getHomeTrip(tripQuantity: number): Observable<Trip[]> {
    return this.$http.get<Trip[]>(`${globals.$api}${globals.$tripsHome}${globals.$tripsquantity}${tripQuantity}`);
  }
}
