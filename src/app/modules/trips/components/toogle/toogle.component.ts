import { Component, OnInit, Input } from '@angular/core';
import { Trip } from 'src/app/_shared/models';

@Component({
  selector: 'app-toogle',
  templateUrl: './toogle.component.html',
  styleUrls: ['./toogle.component.scss']
})
export class ToogleComponent implements OnInit {
  @Input() item: Trip[];
  @Input() index: number;
  public isCollapsed = true;

  constructor() { }

  ngOnInit() {
  }

}
