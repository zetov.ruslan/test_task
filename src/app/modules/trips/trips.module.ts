import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripsComponent } from './pages/trips/trips.component';
import { TripsRoutingModule } from './trips-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToogleComponent } from './components/toogle/toogle.component';


@NgModule({
  declarations: [TripsComponent, ToogleComponent],
  imports: [
    CommonModule,
    TripsRoutingModule,
    NgbModule
  ]
})
export class TripsModule { }
