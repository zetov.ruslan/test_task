import { Component, OnInit } from '@angular/core';
import { TripService } from 'src/app/_shared/services';
import { Trip } from 'src/app/_shared/models';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {

  public trips$: Observable<Trip[]> = of([]);

  constructor(
    private $tripService: TripService
  ) { }

  ngOnInit() {
    this.trips$ = this.$tripService.getTrips();
  }

}
