import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { HomeRoutingModule } from './home-routing.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CarouselComponent } from './components/carousel/carousel.component';
import { HomeTripComponent } from './components/home-trip/home-trip.component';
import { EmailComponent } from './components/email/email.component';

@NgModule({
  declarations: [
    HomeComponent,
    CarouselComponent,
    HomeTripComponent,
    EmailComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgbModule,
  ],
  entryComponents: [EmailComponent]
})
export class HomeModule { }
