import { Component, OnInit } from '@angular/core';
import { CarouselEl, Trip } from 'src/app/_shared/models';
import { TripService } from 'src/app/_shared/services';
import { Observable, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailComponent } from '../../components/email/email.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  elements: CarouselEl[] = [
    {
      imageUrl: 'assets/img/slider2.jpg',
      title: '1'
    },
    {
      imageUrl: 'assets/img/slider3.jpg',
      title: '2'
    },
    {
      imageUrl: 'assets/img/slider4.jpg',
      title: '3'
    },
  ];

  public trips$: Observable<Trip[]> = of([]);

  constructor(
    private $modalService: NgbModal,
    private $tripService: TripService
  ) { }

  ngOnInit() {
    this.trips$ = this.$tripService.getHomeTrip(4);
  }

  open(tripTitle: string) {
    const modalRef = this.$modalService.open(EmailComponent);
    modalRef.componentInstance.name = tripTitle;
  }

}
