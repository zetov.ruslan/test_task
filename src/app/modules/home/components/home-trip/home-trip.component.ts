import { Component, OnInit, Input } from '@angular/core';
import { Trip } from 'src/app/_shared/models';

@Component({
  selector: 'app-home-trip',
  templateUrl: './home-trip.component.html',
  styleUrls: ['./home-trip.component.scss']
})
export class HomeTripComponent implements OnInit {
  @Input() trip: Trip[];

  constructor() { }

  ngOnInit() {
  }

}
