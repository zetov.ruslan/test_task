import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { CarouselEl } from 'src/app/_shared/models/index';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  providers: [NgbCarouselConfig]
})
export class CarouselComponent implements OnInit {
  @Input() carouselEl: CarouselEl[];

  constructor(config: NgbCarouselConfig) {
    config.interval = 10000;
  }

  ngOnInit() {
  }

}
