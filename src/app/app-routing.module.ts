import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('src/app/modules/home/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'trips_list',
    loadChildren: () => import('src/app/modules/trips/trips.module').then(mod => mod.TripsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
